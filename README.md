# README

This repo demonstrates threading issue in gitlab-experiment.

## How to start

``` bash
bin/rails db:setup
bin/rails r '1000.times { User.create! }'

redis-cli flushall
bin/rails r 'User.pluck(:id).each { |user_id| DemoJob.perform_async(user_id) }'
bin/bundle exec sidekiq
```

At some point you will see:

```
2022-02-26T16:38:54.032Z pid=20669 tid=nhd class=DemoJob jid=9fd6276544e6a533da5a6181 elapsed=0.028 INFO: fail
2022-02-26T16:38:54.033Z pid=20669 tid=nhd WARN: {"context":"Job raised exception","job":{"retry":true,"queue":"default","args":[966],"class":"DemoJob","jid":"9fd6276544e6a533da5a6181","created_at":1645893525.5731983,"enqueued_at":1645893525.5732093},"jobstr":"{\"retry\":true,\"queue\":\"default\",\"args\":[966],\"class\":\"DemoJob\",\"jid\":\"9fd6276544e6a533da5a6181\",\"created_at\":1645893525.5731983,\"enqueued_at\":1645893525.5732093}"}
2022-02-26T16:38:54.033Z pid=20669 tid=nhd WARN: Gitlab::Experiment::NestingError: unable to nest demo within demo:
2022-02-26T16:38:54.033Z pid=20669 tid=nhd WARN: /home/ixti/gl-experiment-threading-issue/config/initializers/gitlab_experiment.rb:136:in `block (2 levels) in <main>'
/home/ixti/.gem/ruby/3.0.3/gems/gitlab-experiment-0.7.0/lib/gitlab/experiment/nestable.rb:13:in `instance_exec'
/home/ixti/.gem/ruby/3.0.3/gems/gitlab-experiment-0.7.0/lib/gitlab/experiment/nestable.rb:13:in `nest_experiment'
/home/ixti/.gem/ruby/3.0.3/gems/gitlab-experiment-0.7.0/lib/gitlab/experiment/nestable.rb:34:in `push'
/home/ixti/.gem/ruby/3.0.3/gems/gitlab-experiment-0.7.0/lib/gitlab/experiment/nestable.rb:19:in `manage_nested_stack'
/home/ixti/.gem/ruby/3.0.3/gems/activesupport-7.0.2.2/lib/active_support/callbacks.rb:127:in `block in run_callbacks'
/home/ixti/.gem/ruby/3.0.3/gems/activesupport-7.0.2.2/lib/active_support/callbacks.rb:138:in `run_callbacks'
/home/ixti/.gem/ruby/3.0.3/gems/gitlab-experiment-0.7.0/lib/gitlab/experiment.rb:158:in `run'
/home/ixti/gl-experiment-threading-issue/app/jobs/demo_job.rb:8:in `perform'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:196:in `execute_job'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:164:in `block (2 levels) in process'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/middleware/chain.rb:133:in `invoke'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:163:in `block in process'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:136:in `block (6 levels) in dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/job_retry.rb:114:in `local'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:135:in `block (5 levels) in dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/rails.rb:14:in `block in call'
/home/ixti/.gem/ruby/3.0.3/gems/activesupport-7.0.2.2/lib/active_support/execution_wrapper.rb:92:in `wrap'
/home/ixti/.gem/ruby/3.0.3/gems/activesupport-7.0.2.2/lib/active_support/reloader.rb:72:in `block in wrap'
/home/ixti/.gem/ruby/3.0.3/gems/activesupport-7.0.2.2/lib/active_support/execution_wrapper.rb:92:in `wrap'
/home/ixti/.gem/ruby/3.0.3/gems/activesupport-7.0.2.2/lib/active_support/reloader.rb:71:in `wrap'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/rails.rb:13:in `call'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:131:in `block (4 levels) in dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:257:in `stats'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:126:in `block (3 levels) in dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/job_logger.rb:13:in `call'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:125:in `block (2 levels) in dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/job_retry.rb:81:in `global'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:124:in `block in dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/job_logger.rb:39:in `prepare'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:123:in `dispatch'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:162:in `process'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:78:in `process_one'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/processor.rb:68:in `run'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/util.rb:56:in `watchdog'
/home/ixti/.gem/ruby/3.0.3/gems/sidekiq-6.4.1/lib/sidekiq/util.rb:65:in `block in safe_thread'
```

## Generated with

``` bash
rails new gl-experiment-threading-issue --database=postgresql \
  --skip-action-mailer \
  --skip-action-mailbox \
  --skip-action-text \
  --skip-active-storage \
  --skip-action-cable \
  --skip-asset-pipeline \
  --skip-active-job \
  --skip-test \
  --skip-system-test \
  --skip-hotwire \
  --skip-javascript \
  --skip-jbuilder
```
