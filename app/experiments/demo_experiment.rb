# frozen_string_literal: true

class DemoExperiment < ApplicationExperiment
  default_rollout :percent, include_control: false

  variant(:informal) { "Hi!" }
  variant(:pirate)   { "Ahoy!" }
end
