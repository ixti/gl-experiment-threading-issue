# frozen_string_literal: true

class DemoJob
  include Sidekiq::Worker
  include Gitlab::Experiment::Dsl

  def perform(user_id)
    logger.debug(experiment(:demo, actor: User.find(user_id)).run)
  end
end
